module gitlab.com/opennota/widdly

go 1.15

require (
	github.com/aws/aws-sdk-go v1.37.8
	github.com/boltdb/bolt v1.3.1
	github.com/daaku/go.zipexe v1.0.1
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
)
